/*
 * Dimmer.c
 *
 * Created: 04.08.2012 17:30:20
 *  Author: Lorenz "Gustav" Roos
 */


#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#define __DELAY_BACKWARD_COMPATIBLE__
#define F_CPU 8000000UL  // 8 MHz
#include <util/delay.h>


int timer0Count = 0;
int twentyLampsCounter = 0;


/*
*
* Struct: lampTime
* port: which port the lamp is connected to
* mask: mask to turn on the lamp
* milliseconds: when to turn on
*/
typedef struct
{
	int port;
	uint8_t mask;
	int microsec;
}LampTime;

LampTime twentyLamps[4];


/*
*
* Compare function to sort an array with lampTime structs
*/
int compare(const void *s1, const void *s2) {
	LampTime *lt1 = (LampTime *)s1;
	LampTime *lt2 = (LampTime *)s2;

	return (lt2->microsec - lt1->microsec);
}



/*
*
* Init Interrupts
*
*/
void InitINT0()
{
	// Enable INT0 External Interrupt
	EIMSK |= 1<<INT0;

	// ISCn1 = 1 ISCn0 = 0  The falling edge of INTn generates asynchronously an interrupt request
	EICRA |= 0<<ISC00;
	EICRA |= 1<<ISC01;
}

void InitCount0()
{
	// Timer 0 konfigurieren
	TCCR0A = (1<<WGM01); // CTC Modus

	TCCR0B |= (1<<CS00); // Prescaler 0
	// ((1000000/8)/1000) = 125
	OCR0A = 160-1;

}



int main(void)
{
	//Init Timer
	InitINT0();
	InitCount0();
	DDRD  = 0b11111110;		// set PD0 to input
    PORTD = 0b00000001;		// set PD0 to high

	DDRB  = 0b11111111;		// set PortB to Output
	PORTB = 0b00000000;		// set PortB to LOW

	/* Adding some dummy entries */
	twentyLamps[0].port = 0;
	twentyLamps[0].mask = 0b11111110;
	twentyLamps[0].microsec = 1000;
	twentyLamps[1].port = 0;
	twentyLamps[1].mask = 0b11111101;
	twentyLamps[1].microsec = 2000;
	twentyLamps[2].port = 0;
	twentyLamps[2].mask = 0b11111011;
	twentyLamps[2].microsec = 4000;
	twentyLamps[3].port = 0;
	twentyLamps[3].mask = 0b11110111;
	twentyLamps[3].microsec = 8000;

	/* Sortin the array by millisecond */
	qsort(twentyLamps,4,sizeof(LampTime),compare);

	sei();							// Enable Interrupt

    while(1){

	}
}

ISR(INT0_vect)
{
	cli();

	// initialize counter
	TCNT0  = 0;

	// Compare Interrupt erlauben
	 TIMSK0 |= (1<<OCIE0A);

	sei();
}

ISR (TIMER0_COMPA_vect)
{


	timer0Count++;					//increment Timer

	if(twentyLamps[twentyLampsCounter].microsec <= timer0Count * 20) {

		/* turn lamp on */
		if (twentyLamps[twentyLampsCounter].port == 0) {
	
			PORTB |= twentyLamps[twentyLampsCounter].mask;
		
		}
		
		//twentyLamps[twentyLampsCounter].port |= twentyLamps[twentyLampsCounter].mask;
		/* update Counter to check the next element */
		twentyLampsCounter++;
	}

	if (timer0Count > 479){
		PORTD = 0b00000001;
		PORTB = 0b00000000;

		timer0Count = 0;			//Reset Timer Counter
		twentyLampsCounter = 0;     //Reset Array Counter

		TIMSK0 |= (0<<OCIE0A);

	}

}