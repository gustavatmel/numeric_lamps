#include <stdio.h>
#include <stdlib.h>
/**
*
*
**/
struct lampTime {
	uint8_t port;
	uint8_t mask;
	int milliseconds;
};

int compare(const void *s1, const void *s2) {
	struct lampTime *lt1 = (struct lampTime *)s1;
	struct lampTime *lt2 = (struct lampTime *)s2;

	return (lt2->milliseconds - lt1->milliseconds);
}

/*
 * print a table for Fahrenheit to Celsius
 * from 0 F to 300 F
 */
int main(void)
{

	struct lampTime twentyLamps[] = {
		{0b11111111,0b00000001,3000},
		{0b11111111,0b00000010,2500},
		{0b11111111,0b00000100,1750},
		{0b11111111,0b00001000,6089}
	};

	int i;
	printf("unsorted\n");
	for(i=0;i<4;i++) {
		printf("%u\t%u\t%d\n",twentyLamps[i].port,twentyLamps[i].mask,twentyLamps[i].milliseconds);
	}

	qsort(twentyLamps,4,sizeof(struct lampTime),compare);

	printf("\n\nsorted\n");

	for(i=0;i<4;i++) {
		printf("%u\t%u\t%d\n",twentyLamps[i].port,twentyLamps[i].mask,twentyLamps[i].milliseconds);
	}

	/*
	 * say goodbye
	 */
	exit(0);
}


